import java.util.Random;
public class Die{
	private int faceValue;
	private Random dieRoll;
	
	public Die(){
		this.faceValue = 1;
		this.dieRoll = new Random();
	}
	
	public void roll(){
		this.faceValue = dieRoll.nextInt(6)+1;
	}
	
	public int getFaceValue(){
		return this.faceValue;
	}
	
	public String toString(){
		return "face value: "+this.faceValue;
	}
	
	
	
	
	
}