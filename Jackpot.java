public class Jackpot{
	public static void main(String[] args){
		java.util.Scanner reader = new java.util.Scanner(System.in);
		
		boolean continueStatus = true;
		
		int winCount = 0;
		
		while (continueStatus){
			System.out.println("Welcome to this Jackpot game!");
		
			Board board = new Board();
		
			boolean gameOver = false;
		
			int numOfTilesClosed = 0;
		
			while (!gameOver){
				System.out.println(board);
				gameOver  = board.playATurn();
				numOfTilesClosed += 1;
			}
		
			if (numOfTilesClosed >= 7){
				System.out.println("You reached the Jackpot! You win!");
				winCount +=1;
			}
			else {
				System.out.println("You lost...");
			}
			System.out.println("Continue?(yes/no)");
			
			String answer = reader.next();
			
			if (answer.equals("no")){
				System.out.println("Your win count: "+winCount+", congrats!");
				continueStatus = false;
			}
		}
	
	}
}