public class Board{
	private Die die1;
	private Die die2;
	private boolean[] tiles;
	
	public Board(){
		this.die1 = new Die();
		this.die2 = new Die();
		this.tiles = new boolean[12];
	}
	
	public boolean playATurn(){
		this.die1.roll();
		this.die2.roll();
		
		System.out.println(this.die1);
		System.out.println(this.die2);
		
		int sumOfDice = die1.getFaceValue() + die2.getFaceValue();
		
		if(!tiles[sumOfDice-1]){
			tiles[sumOfDice-1] = true;
			System.out.println("Closing tile equal to sum: "+sumOfDice);
			return false;
		}
		else {
			if(!tiles[this.die1.getFaceValue()]){
				tiles[this.die1.getFaceValue()] = true;
				System.out.println("Closing tile with same value as die1: "+this.die1.getFaceValue());
				return false;
			}
			else if(!tiles[this.die1.getFaceValue()]){
				tiles[this.die1.getFaceValue()] = true;
				System.out.println("Closing tile with same value as die1: "+this.die1.getFaceValue());
				return false;
			}
			else if(!tiles[this.die2.getFaceValue()]){
				tiles[this.die2.getFaceValue()] = true;
				System.out.println("Closing tile with same value as die2: "+this.die2.getFaceValue());
				return false;
			}
			else {
				System.out.println("All the tiles for these values are already shut");
				return true;
			}
		}
	}
	
	public String toString(){
		String tilesRep = "";
		for(int i=0; i<this.tiles.length; i++){
			if(tiles[i]){
				tilesRep = tilesRep+ "X ";
			}
			else {
				tilesRep = tilesRep+(i+1)+" ";
			}
		}
		return tilesRep;
	}
}